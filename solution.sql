mysql -u root -p

CREATE DATABASE enrollment_db;

USE enrollment_db;

CREATE TABLE teachers (
  teacher_id INT NOT NULL AUTO_INCREMENT,
  teacher_name VARCHAR(50) NOT NULL,
  PRIMARY KEY(teacher_id)
);

CREATE TABLE courses (
  course_id INT NOT NULL AUTO_INCREMENT,
  teacher_id INT NOT NULL,
  course_name VARCHAR(50) NOT NULL,
  PRIMARY KEY(course_id),
  CONSTRAINT fk_teacher_id FOREIGN KEY (teacher_id) REFERENCES teachers(teacher_id) ON UPDATE CASCADE
  ON DELETE RESTRICT
);

CREATE TABLE students (
  student_id INT NOT NULL AUTO_INCREMENT,
  student_name VARCHAR(50) NOT NULL,
  PRIMARY KEY(student_id)
);

CREATE TABLE student_courses (
  course_id INT NULL,
  student_id INT NULL,
  CONSTRAINT fk_course_id FOREIGN KEY (course_id) REFERENCES courses(course_id) 
  ON UPDATE CASCADE
  ON DELETE RESTRICT,
  CONSTRAINT fk_student_id FOREIGN KEY (student_id) REFERENCES students(student_id) 
  ON UPDATE CASCADE
  ON DELETE RESTRICT
);

INSERT INTO students (student_name)
VALUES ("Ben"),
("Sylvan"),
("Charles"),
("Terence"),
("Billy"),
("Jino"),
("Chris");

INSERT INTO teachers (teacher_name)
VALUES ("Alan"),
("Judy"),
("Arvin");

INSERT INTO courses (course_name, teacher_id)
VALUES ()

INSERT INTO student_courses
